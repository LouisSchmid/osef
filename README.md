# Outsight Code Samples

## Description

This repository contains code samples to make the most of an Outsight Augmented Lidar Box (ALB).

It is meant to help anyone to quickly begin to use ALB data in his own application.

Source code and usage instructions are provided in sub-folders depending on the language.
The feature coverage is sum-up below:

 Language           | Read & parse live stream     | Read & parse recorded stream | Control and configure ALB
 ---                | ---                          |  ---                         | ---
 [C++](cpp)       | :white_check_mark: supported | :white_check_mark: supported | :construction: planned support
 [Python3](python)  | :white_check_mark: supported | :white_check_mark: supported | :white_check_mark: supported

Tested platform:

 Platform                        | Status
 ---                             | ---
 Ubuntu 20.04, gcc 9, python 3.8 | :white_check_mark: tested
 Windows                         | :construction: to be tested

## OSEF - the Serialization format

OSEF stands for Open SErialization Format, it is an outsight-defined serialization format used to efficiently transmit and save output data.

An Outsight Augmented Lidar Box (ALB) transmits the output data using OSEF format, via a TCP stream.
This stream can be handled directly by the application, or saved as an osef file to be processed offline.

### TLV (Type-Length-Value)

OSEF is based on TLV-encoding (https://en.wikipedia.org/wiki/Type-length-value).

The structure of the data is a tree with a single root.
Each element contains:

field   | size    | description 
 ---    | ---     | --- 
Type    | 4 Bytes | Describes the application meaning of the data, and how value can be interpreted
Length  | 4 Bytes | Little-endian 32 bits unsigned integer, giving the size N of the Value fields (in bytes)
Value   | N Bytes | Depending on the type, can be either a concatenation of sub-TLVs, or a raw buffer
Padding | M Bytes | To get a 4 Bytes alignment, a TLV will be padded with 0 to 3 bytes. It will happen if N is not a multiple of 4 (for instance, if N=6, then M=2). 

An example code to parse this structure are available int the following files [tlvCommon.h](cpp/external/osef/include/tlvCommon.h),
[tlvParser.h](cpp/external/osef/include/tlvParser.h), [tlvParser.cpp](cpp/external/osef/src/tlvParser.cpp).

### Outsight types

To be able to parse an osef file or stream, one must know each type definition to be able to interpret the 'Value' field.
So the description of each type is crucial. It is done in the following file:
[osefTypes.h](cpp/external/osef/include/osefTypes.h)

To get an idea of how the real data is encoded, this is an example of how we represent an augmented cloud, 
which is a point cloud with several optional properties attached to each point.

```
                                  +------------------+
                                  |  AugmentedCloud  |
                                  +------------------+
                                           |
         +--------------------+------------+-------+-----------------------+------------+---+ ... --+----+
         |                    |                    |                       |                |       |    |
+----------------+   +----------------+   +----------------+   +----------------------+
| NumberOfPoints |   | NumberOfLayers |   | Reflectivities |   | CartesianCoordinates |  (other optional properties)
+----------------+   +----------------+   +----------------+   +----------------------+
       2345                 123           <buffer of 2345 B>    <buffer of 2345*12 B>

```

We can see that the tree does not go as deep as describing the coordinates of each point.
On the contrary it stops at some point, the application code must interpret the raw bytes buffer according to the type.
