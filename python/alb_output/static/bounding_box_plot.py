"""3D Plot of tracked object bounding boxes from a file
or stream with Outsight Serialization format (.osef extension).
"""
# Standard imports
import random
from argparse import ArgumentParser
from itertools import combinations, product

# Third party imports
import numpy as np
import plotly.graph_objects as go
import osef
from osef import osef_frame


def plot_bounding_boxes(osef_path):
    """Parse and plot bounding boxes from an osef file.

    :param osef_path: path or url to the osef file or stream.
    :return: None
    """
    # parse osef stream
    first_frame_dict = None
    for frame_dict in osef.parser.parse(osef_path, first=1):
        first_frame_dict = frame_dict
        break
    else:
        return

    # extract geometric data
    tracked_objects = osef_frame.TrackedObjects(first_frame_dict)
    shapes = tracked_objects.bounding_boxes
    color_map = [f'{"#%06x" % random.randint(0, 0xFFFFFF)}' for _ in shapes]
    rotations = [pose.rotation for pose in tracked_objects.poses]
    translations = [pose.translation for pose in tracked_objects.poses]
    are_oriented = [
        properties.oriented for properties in tracked_objects.object_properties
    ]

    # plot bounding boxes
    bboxes = []
    for shape, is_oriented, rotation, translation, color in zip(
        shapes, are_oriented, rotations, translations, color_map
    ):
        if is_oriented:
            edges = generate_hyper_rectangle_edges(shape, rotation, translation)

            for edge in edges:
                bboxes.append(
                    go.Scatter3d(
                        # 8 vertices of a cube
                        x=edge[0],
                        y=edge[1],
                        z=edge[2],
                        mode="lines",
                        line=dict(color=color, width=2),
                    )
                )
        else:
            x_grid, y_grid, z_grid = generate_cylinder_grids(
                translation, shape[0] / 2, shape[2]
            )
            bboxes.append(
                go.Surface(
                    x=x_grid,
                    y=y_grid,
                    z=z_grid,
                    colorscale=[[0, color], [1, color]],
                    opacity=0.5,
                    showscale=False,
                )
            )

    layout = go.Layout(
        scene=dict(aspectmode="data"),  # data: equal aspect ratios
        showlegend=False,
    )
    fig = go.Figure(
        data=bboxes,
        layout=layout,
    )
    fig.update_coloraxes(showscale=False)
    fig.show()


def generate_cylinder_grids(translation: np.ndarray, radius: float, height: float):
    """Generate cylinder grid along the z axis.

    :param translation: coordinates of the cylinder in the XY plan. [x, y]
    :param radius: of the cylinder.
    :param height: of the cylinder
    :return: tuples of grids (x_grid, y_grid, z_grid)
    """
    z_heights = np.linspace(0, height, 50)
    theta = np.linspace(0, 2 * np.pi, 50)
    theta_grid, z_grid = np.meshgrid(theta, z_heights)
    x_grid = radius * np.cos(theta_grid) + translation[0]
    y_grid = radius * np.sin(theta_grid) + translation[1]
    return x_grid, y_grid, z_grid


def generate_hyper_rectangle_edges(
    shape: np.ndarray, rotation: np.ndarray, translation: np.ndarray
):
    """Generate hyper rectangle edges.

    :param shape: of the rectangle [width, length, height]
    :param rotation: of the rectangle
    relatively to the absolute referential. (3x3 matrix)
    :param translation: of the rectangle
    relatively to the absolute referential. (3d vector)
    :return: list of edges: [[corner_1, corner_2], [corner_1, corner_3] ...]
    with corner_x=[x, y, z]
    """
    width = [-shape[0] / 2, shape[0] / 2]
    length = [-shape[1] / 2, shape[1] / 2]
    height = [-shape[2] / 2, shape[2] / 2]
    edges = []
    for corner_1, corner_2 in combinations(
        np.array(list(product(width, length, height))), 2
    ):
        # a vector is an edge when it is along 1 axis.
        # example of edge along y:
        #   corner_1 - corner_2 = [0, X, 0]
        if len(np.where(np.abs(corner_1 - corner_2) == 0)[0]) == 2:
            first_corner = transform_point(corner_1, rotation, translation).reshape(
                3, 1
            )
            second_corner = transform_point(corner_2, rotation, translation).reshape(
                3, 1
            )
            edges.append(np.hstack((first_corner, second_corner)))
    return edges


def transform_point(
    point_coordinates: np.ndarray, rotation: np.ndarray, translation: np.ndarray
):
    """Apply a rotation then a translation to a coordinate vector.

    :param point_coordinates: 3D coordinate vector of a point
    :param rotation: matrix (3x3)
    :param translation: vector (1x3)
    :return: rotated then translated 3D vector
    """
    rotated_points = np.dot(rotation, point_coordinates)
    return rotated_points + translation


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        description="3D Plot of tracked object bounding boxes from a "
        "file or stream with Outsight Serialization format (.osef extension)."
    )
    arg_parser.add_argument(
        "input",
        type=str,
        help="File to be plotted (example: file.osef). "
        "TCP stream are also accepted on the form of tcp://host[:port].",
    )
    args = arg_parser.parse_args()

    plot_bounding_boxes(args.input)
