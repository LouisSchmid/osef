"""Lists lidar poses from a osef file or stream to a csv file."""
# Standard imports
from argparse import ArgumentParser
from datetime import datetime
from typing import Iterator, List

# Third party imports
from osef import osef_frame

# Local imports
from alb_output.common.csv_maker import make_csv


def list_lidar_poses(frame: dict) -> Iterator[List]:
    """Iterator that return the pose of the lidar as a dict.
    Can be used as a callback of make_csv to generate a csv file with all the poses of the lidar.

    :param frame: dictionary containing the parsed values of a tlv scan frame.
    :return: a dictionary (raw of the csv) with the pose of the lidar.
    """

    # Extract relevant fields from the OSEF
    scan_frame = osef_frame.ScanFrame(frame)
    lidar_pose = scan_frame.pose

    # Build a new row with all the information
    row = {
        "timestamp": datetime.utcfromtimestamp(scan_frame.timestamp),
        "position_x": lidar_pose.translation[0],
        "position_y": lidar_pose.translation[1],
        "position_z": lidar_pose.translation[2],
        "rotation_xx": lidar_pose.rotation[0][0],
        "rotation_xy": lidar_pose.rotation[0][1],
        "rotation_xz": lidar_pose.rotation[0][2],
        "rotation_yx": lidar_pose.rotation[1][0],
        "rotation_yy": lidar_pose.rotation[1][1],
        "rotation_yz": lidar_pose.rotation[1][2],
        "rotation_zx": lidar_pose.rotation[2][0],
        "rotation_zy": lidar_pose.rotation[2][1],
        "rotation_zz": lidar_pose.rotation[2][2],
    }

    yield row


# As a script, will unpack all the files passed as command line arguments
if __name__ == "__main__":
    parser = ArgumentParser(
        description="Decode osef file in from ALB in moving lidar "
        "application and output csv with poses information."
    )
    parser.add_argument(
        "input",
        type=str,
        help="File to be decoded (example: file.osef). "
        "Tcp stream are accepted on the form of tcp://host:port.",
    )
    parser.add_argument(
        "output",
        metavar="output.csv",
        type=str,
        nargs="?",
        help="Path to csv file to save the output. Printed to stdout if omitted.",
    )
    args = parser.parse_args()

    make_csv(args.input, list_lidar_poses, args.output)
