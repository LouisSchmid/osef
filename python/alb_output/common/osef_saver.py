"""As a script, will save tcp stream from an ALB to a file."""
import logging
import time
from argparse import ArgumentParser

import osef

logging.root.setLevel(logging.INFO)


def save_osef(input_stream: str, output_path: str):
    """Save tcp stream from an ALB to a file.

    :param input_stream: stream to be saved. For instance tcp//192.168.2.2:11120.
    :param output_path: Name of the saved osef file.
    :return: None
    """

    logging.info(f"Opening stream {input_stream} and saving to {output_path}")
    written_bytes = osef.saver.save_osef_from_tcp(
        input_stream, output_path, print_progress=True
    )
    logging.info(f"Wrote {osef.saver.pretty_size(written_bytes)}")


if __name__ == "__main__":
    parser = ArgumentParser(description="Save tcp stream from an ALB to a file. ")
    parser.add_argument(
        "input",
        metavar="tcp://host[:port]",
        type=str,
        help="Stream to be saved. For instance tcp//192.168.2.2:11120.",
    )
    parser.add_argument(
        "output",
        metavar="output.osef",
        type=str,
        nargs="?",
        default=time.strftime("alb-%Y%m%d-%H%M%S.osef"),
        help="Name of the saved osef file.",
    )
    args = parser.parse_args()
    save_osef(args.input, args.output)
