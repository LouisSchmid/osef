"""Stream OSEF files on TCP sockets.
Packets will be sent at the right timing,
all stream being synchronized with each other
"""
import logging
from argparse import ArgumentParser
import json
import sys
import jsonschema
from jsonschema import validate

from osef.multi_streamer import MultiOsefStreamer

JSON_OSEF_LIST_SCHEMA = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "osef_path": {"type": "string"},
            "ip": {"type": "string"},
            "port": {"type": "number"},
        },
    },
}

logging.root.setLevel(logging.INFO)


def validate_json(json_data):
    """Check for json data against the json schema"""
    try:
        validate(instance=json_data, schema=JSON_OSEF_LIST_SCHEMA)
    except jsonschema.exceptions.ValidationError as err:
        return False, err
    return True, None


def stream_osefs(json_path, overwrite_timestamp=False):
    """Parse the json config file and run the multistreamer"""
    osef_dict = {}
    with open(json_path, "r") as config_file:
        osef_dict = json.load(config_file)
        success, err = validate_json(osef_dict)
        if not success:
            print("Error parsing the json file:", err)
            sys.exit(1)

    with MultiOsefStreamer(osef_dict, overwrite_timestamp) as multi_osef_streamer:
        multi_osef_streamer.run()


if __name__ == "__main__":
    parser = ArgumentParser(description="Stream multiple OSEF files on TCP sockets.")
    parser.add_argument(
        "--json",
        type=str,
        help="JSON configuration file",
    )
    parser.add_argument(
        "--overwrite_timestamp",
        action="store_true",
        help="If set, the osef streams are timestamped with current time",
    )
    args = parser.parse_args()
    if not args.json:
        print("Missing required arguments")
        parser.print_help()
        sys.exit(1)
    stream_osefs(args.json, args.overwrite_timestamp)
    sys.exit(0)
