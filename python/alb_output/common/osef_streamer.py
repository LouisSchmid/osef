"""Stream an OSEF file on a TCP socket.
Packets will be sent at the right timing,
to be in the same conditions as when we receive
the stream from an ALB.
"""
from argparse import ArgumentParser
from osef import streamer


def stream_osef_file(
    file_path: str, ip_address: str, port: int = 11120, repeat: bool = False
):
    """Stream an osef file on a TCP socket with the right timings.

    :param file_path: local path to the osef file to stream.
    :param ip_address: hostname or IP address
    :param port: to be used (typically 11120)
    :param repeat: stream the osef in a loop.
    """
    streamer.stream_osef_file(file_path, ip_address, port, repeat)


if __name__ == "__main__":
    parser = ArgumentParser(description="Stream an OSEF file on a TCP socket.")
    parser.add_argument(
        "osef_filepath",
        type=str,
        help="Filepath to the osef file to stream (example: path/to/record.osef)",
    )
    parser.add_argument(
        "ip",
        type=str,
        help="Hostname or IP address to which the TCP stream will be sent to.",
    )
    parser.add_argument(
        "--port",
        type=int,
        default=11120,
        help="Port number to which the TCP stream will be sent to.",
    )

    parser.add_argument(
        "--repeat",
        action="store_const",
        const=True,
        default=False,
        help="Stream the osef file in a loop.",
    )
    args = parser.parse_args()
    stream_osef_file(args.osef_filepath, args.ip, args.port, args.repeat)
