"""Converter from osef to csv."""
import contextlib
import csv
import sys
from typing import Optional

from osef import parser


@contextlib.contextmanager
def file_writer(file_name=None):
    """Write to file or to stdout if file_name is equal to None"""
    # Create writer object based on file_name
    writer = open(file_name, "w") if file_name is not None else sys.stdout
    # yield the writer object for the actual use
    yield writer

    # called on context exit: If it is file, then close the writer object
    if file_name is not None:
        writer.close()


def make_csv(input_path: str, get_rows_from_frame, output_path: Optional[str] = None):
    """Utility to convert an osef available in input_path
    (./my_record.osef or tcp://alb_ip) to a csv representation
    The csv representation is application specific.

    :param input_path: osef file path.
    :param get_rows_from_frame: callback in charge of generating an array of rows
    (each row represented as a dictionary) to add to the csv file.
    :param output_path: The CSV file will be written to 'outputPath',
    or just printed if outputPath is omitted
    :return: None
    """

    with file_writer(output_path) as out_file:
        writer = None
        for idx, tree in enumerate(parser.parse(input_path)):

            # call rows_from_frame callback
            # where application can define the fields it is interested in
            rows_iterator = get_rows_from_frame(tree)

            # Automatically adds frame_number column
            rows = [{**{"frame_number": idx}, **user_row} for user_row in rows_iterator]

            # Initialization case: write csv header
            if len(rows) > 0 and writer is None:
                writer = csv.DictWriter(out_file, rows[0].keys())
                writer.writeheader()

            # Write new rows
            for row in rows:
                writer.writerow(row)
