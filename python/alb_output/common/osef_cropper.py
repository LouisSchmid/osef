"""Crop sub-OSEF from OSEF input."""
# Standard imports
from argparse import ArgumentParser

# OSEF imports
import osef


def count_frames(osef_file):
    """Count number of OSEF frames."""
    with osef.parser.OsefStream(osef_file) as osef_stream:
        return len([None for _ in osef.parser.get_tlv_iterator(osef_stream)])


def crop_osef(file: str, output: str, first: int, last: int):
    """Crop/Extract frames in range [first, last] of the input OSEF."""
    print("##### OSEF Cropper #####")
    print(
        f"Crop sub-OSEF for frames [{first}, {last}] from {file} ({count_frames(file)} frames)"
    )

    with open(output, "wb") as output_file:
        for frame_index, frame_dict in enumerate(osef.parser.parse(file)):
            if frame_index < first:
                continue
            if last is not None and frame_index > last:
                return
            output_file.write(osef.packer.pack(frame_dict))


if __name__ == "__main__":

    arg_parser = ArgumentParser(description="Crop a sub-OSEF file from an OSEF input.")
    arg_parser.add_argument(
        "input",
        metavar="file.osef",
        type=str,
        help="Input file to crop the sub-OSEF from.",
    )
    arg_parser.add_argument(
        "output",
        metavar="file.osef",
        type=str,
        help="Output path for the cropped sub-OSEF.",
    )
    arg_parser.add_argument(
        "--first",
        metavar="N",
        type=int,
        default=0,
        help="Index of the first frame",
    )
    arg_parser.add_argument(
        "--last",
        metavar="M",
        type=int,
        default=None,
        help="Index of the last frame",
    )

    args = arg_parser.parse_args()
    crop_osef(args.input, args.output, args.first, args.last)
