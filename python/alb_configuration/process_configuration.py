"""Here is an example to show how to use the ALB REST API to:
upload files, configure and start process.

Here are the different steps to configure an ALB:
* upload pcap file to replay (if running from file)
* upload lidar pose
* upload reference map (if provided)
* upload zone definitions (if static LiDAR)
* upload lidar configuration file (Ouster LiDAR only)
* set process configuration
* start processing
* check that the processing is running

Once the process launched, you may verify the results in the WebApp:
https://YOUR_ALB_IP

See REST API Description:
https://app.swaggerhub.com/apis-docs/Outsight/alb_api

or in the alb directly
https://YOUR_ALB_IP/api/v1/
"""
import json
import logging
import pathlib
from argparse import ArgumentParser

import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
logging.root.setLevel(logging.INFO)


def configure_alb(
    alb_ip: str,
    config_path: str,
    pcap_path: str = None,
    pose_path: str = None,
    map_path: str = None,
    zones_path: str = None,
    lidar_config_path: str = None,
):
    """Configure then start an ALB process

    :param alb_ip: ALB IP address.
    :param config_path: to the file containing the body of the PUT request
    to send to the REST API of the ALB.
    :param pcap_path: path to the pcap record which contains the
    UDP packets from a LiDAR.
    :param pose_path: path to the LiDAR pose file.
    :param zones_path: path to the json file containing tracking zone definitions.
    :param map_path: path to the ply file containing the reference map.
    :param lidar_config_path: path to the lidar configuration file (necessary for Ouster LiDAR)
    """
    with open(config_path, "r") as file:
        config = json.load(file)

    api_url = f"https://{alb_ip}/api/v1"
    # Upload a pcap file to be replayed
    if pcap_path:
        upload_file(
            api_url,
            pcap_path,
            file_category="records",
            file_name=config["lidars"][0].get("filename"),
        )

    # Upload the LiDAR relative pose (in absolute frame)
    if pose_path:
        upload_file(
            api_url,
            pose_path,
            file_category="poses",
            file_name="tracking_lidar.pose",
        )
    # Upload LiDAR configuration file
    if lidar_config_path:
        upload_file(
            api_url,
            lidar_config_path,
            file_category="lidar-configs",
            file_name=config["lidars"][0].get("config_filename"),
        )

    # Upload reference map to the ALB.
    # Then this one will be available in the Augmented Studio
    if map_path:
        file_name = pathlib.Path(map_path).name
        upload_file(api_url, map_path, file_category="maps", file_name=file_name)

    if zones_path:
        upload_zones(api_url, zones_path)

    configure_process(api_url, config)
    start_process(api_url)


def upload_file(base_url: str, local_path: str, file_category: str, file_name: str):
    """Upload a file to the ALB from local filesystem.
    It will be deleted before upload if it was uploaded previously.

    :param base_url: base URL of the ALB API
    :param local_path: path to the file on the local filesystem
    :param file_category: Category of the file to upload.
    Available values: [records, maps, lidar-configs, poses, other]
    :param file_name: file name in the ALB. Should match the one given
    in the process configuration.
    If filename already exists, the file won't be uploaded.
    """
    if not pathlib.Path(local_path).is_file():
        raise FileExistsError(f"File {local_path} does not exist.")

    if file_name is None:
        raise RuntimeError(
            f"No filename provided for file category = '{file_category}'"
        )

    # check if file already exists
    response = requests.get(
        f"{base_url}/storage/{file_category}",
        verify=False,
    )
    _check_response(response)

    alb_files = response.json()["files"]
    alb_filenames = [file["name"] for file in alb_files]

    # delete file if it already exists
    if file_name in alb_filenames:
        logging.info(f"{file_name} as it already exits, deleting it before upload.")
        del_repsonse = requests.delete(
            f"{base_url}/storage/{file_category}/{file_name}",
            verify=False,
        )
        _check_response(del_repsonse)

    # upload file to the ALB
    with open(str(local_path), "rb") as file:
        logging.info(f"uploading {file_name}")
        response = requests.post(
            f"{base_url}/storage/{file_category}/{file_name}",
            data=file,
            headers={"Content-Type": "application/octet-stream"},
            verify=False,
        )

    _check_response(response)


def upload_zones(base_url: str, zones_path: str):
    """Upload zone descriptions to ALB

    :param zones_path: path to the json file containing tracking zone definitions."""
    if not pathlib.Path(zones_path).is_file():
        raise FileExistsError(f"File {zones_path} does not exist.")
    # upload file to the ALB
    with open(str(zones_path), "rb") as file:
        logging.info(f"uploading zones {zones_path}")
        zone_def = json.load(file)
        response = requests.put(
            f"{base_url}/processing/zones",
            json=zone_def,
            verify=False,
        )

    _check_response(response)


def configure_process(base_url: str, config: dict):
    """Set processing configuration

    :param base_url: base URL of the ALB API
    :param config: process configuration which will be set.
    """

    logging.info("Setting process configuration")
    response = requests.put(
        f"{base_url}/processing/config",
        json=config,
        verify=False,
    )
    _check_response(response)


def start_process(base_url):
    """start/restart the ALB processing.
    Then check if the process is running.

    :param base_url: base URL of the ALB API
    """
    logging.info("Starting process.")
    response = requests.post(
        f"{base_url}/processing/restart",
        verify=False,
    )
    _check_response(response)

    # check that the process is running
    response = requests.get(
        f"{base_url}/processing/status",
        verify=False,
    )
    _check_response(response)

    if not response.json().get("running"):
        raise RuntimeError(f"ALB process not running: {response.status_code}")


def _check_response(response: requests.Response):
    try:
        response.raise_for_status()
    except requests.HTTPError as err:
        if "detail" in json.loads(response.text):
            logging.error(
                f"request to ALB failed: {response.status_code}.\n"
                f"details: {json.loads(response.text)['detail']}"
            )
        raise err


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Configure ALB using its REST API.")

    arg_parser.add_argument(
        "process_configuration_path",
        metavar="process_config.json",
        type=str,
        help="path to the json file containing the body of the PUT request "
        "to send to the REST API of the ALB.",
    )
    arg_parser.add_argument(
        "--alb-ip",
        default="192.168.2.2",
        type=str,
        help="ALB IP address.",
    )
    arg_parser.add_argument(
        "--pcap-path",
        metavar="record.pcap",
        type=str,
        help="Path to the pcap record which contains the UDP packets from a LiDAR.",
    )
    arg_parser.add_argument(
        "--pose-path",
        metavar="lidar.pose",
        type=str,
        help="Path to the LiDAR pose file.",
    )
    arg_parser.add_argument(
        "--map-path",
        metavar="reference_map.ply",
        type=str,
        help="Path to the ply file containing the reference map.",
    )
    arg_parser.add_argument(
        "--zones-path",
        metavar="zones.json",
        type=str,
        help="Path to the json file containing tracking zone definitions.",
    )
    arg_parser.add_argument(
        "--lidar-config-path",
        metavar="lidar_config.json",
        type=str,
        help="Path to the lidar configuration file (necessary for Ouster LiDAR).",
    )

    args = arg_parser.parse_args()

    configure_alb(
        config_path=args.process_configuration_path,
        alb_ip=args.alb_ip,
        pcap_path=args.pcap_path,
        pose_path=args.pose_path,
        map_path=args.map_path,
        zones_path=args.zones_path,
        lidar_config_path=args.lidar_config_path,
    )
