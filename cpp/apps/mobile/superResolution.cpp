// Local headers
#include "parser/pointCloudParser.h"

// Define the default number of scans for generating the point Cloud.
static constexpr int MAX_SCANS = 30;

int main(int argc, char **argv)
{
	// If called with no argument
	if (argc < 3) {
		printf("[SuperResolution] Usage :\n");
		printf("\t%s /path/to/recordedStream.osef path/to/output.ply\n", argv[0]);
		printf("\t%s /path/to/recordedStream.osef path/to/output.ply number_of_scans\n", argv[0]);
		return -1;
	}

	// Parse input arguments.
	int max_scans = MAX_SCANS;
	if (argc == 4) {
		int scan_arg = atoi(argv[3]);
		if (scan_arg > 0) {
			max_scans = scan_arg;
		}
	}

	PointCloudParser parser(argv[1], argv[2], max_scans);
	return parser.parse();
}