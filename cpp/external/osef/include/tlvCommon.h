#pragma once

#include <cstdint>
#include <cstdio>

namespace Tlv
{
// Type (T of TLV) is encoded on 4 bytes
typedef uint32_t type_t;

// Can be used passed as size argument to API function
constexpr size_t UNLIMITED = UINT32_MAX;

// TLV is padded to 4-octet alignment
constexpr size_t alignmentSize = 4;

// TLV header (TL part)
#pragma pack(push, 1)
struct __attribute__((packed)) header_s {
	type_t type;
	uint32_t length;
};
#pragma pack(pop)

// TLV stucture (warning: the sizeof will _not_ include value, which is of variable size)
#pragma pack(push, 1)
struct __attribute__((packed)) tlv_s {
    private:
	header_s header;
	uint8_t value[]; // Number of bytes is specified by header.length

    public:
	// Return type of TLV
	inline type_t getType() const
	{
		return header.type;
	}

	// Return length of TLV
	inline size_t getLength() const
	{
		return header.length;
	}

	// Return buffer of TLV
	inline const uint8_t *getValue() const
	{
		return value;
	}
};
#pragma pack(pop)

// Returns number of padding bytes to be added depending on non-padded size
static inline size_t getPadding(size_t size)
{
	size_t n = size % alignmentSize;
	return (n == 0) ? 0 : alignmentSize - n;
}

// Return size of TLV, it differs from length since it includes header size and padding
static inline size_t getSize(const tlv_s &tlv)
{
	size_t unpadded_size = sizeof(header_s) + tlv.getLength();
	return unpadded_size + getPadding(unpadded_size);
}

} // namespace Tlv
