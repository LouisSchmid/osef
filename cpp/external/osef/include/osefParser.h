#ifndef _OSEF_PARSER_H
#define _OSEF_PARSER_H

// Standard headers.
#include <string>

// Osef headers.
#include "tcpStream.h"
#include "tlvParser.h"

// Class to parse an osef stream, from file or from live stream.
class OsefParser {
    public:
	OsefParser(const std::string &inputPath, bool autoReconnect = true);

	// Parse the input stream.
	// This function can be overridden for specific cases.
	// returns:
	// 0 in case of success.
	// -1 in error.
	virtual int parse();

    private:
	// Parse the input arguments (Live or record).
	void parseInputArguments(const std::string &inputPath);

	// Parse a live stream coming from an ALB.
	// returns:
	// 0 in case of success.
	// -1 in error.
	int parseLiveStream();

	// Parse a record stream coming from an osef file.
	// returns:
	// 0 in case of success.
	// -1 in error.
	int parseRecordStream();

    protected:
	// Common check before starting parsing.
	// returns:
	// True if parsing is valid.
	// False if not.
	bool checkValidParsing(const Tlv::tlv_s *frame);

	// Connect to the live stream.
	void connectToLiveStream();

	// Parse the Tlv frame.
	// This function is called each time a new frame is received (live) or read (record).
	// WARNING. Virtual function which has to be overrided in the derived class.
	// returns:
	// 0 in case of success.
	// -1 in error.
	virtual int parseFrame(const Tlv::tlv_s *frame) = 0;

    protected:
	std::string input_path;
	bool auto_reconnect;

	TcpStreamReader tcp_reader;
	std::string ip_v4;
	int port;
};

#endif // _OSEF_PARSER_H