#ifndef _PLY_WRITER_H
#define _PLY_WRITER_H

// Eigen headers
#include "Eigen/Core"

// Class to write pointCloud to a PLY file.
class PlyWriter {
    public:
	PlyWriter(const std::string &outputPath);

	// Write point matrix with reflectivity.
	void writePoints(const Eigen::MatrixXf &points, const Eigen::VectorXi &reflectivities);

    private:
	// Write the PLY file header.
	void writePlyHeader();

	// Update the total number of points in the header.
	void updateNumberOfPoints(FILE *fp) const;

    private:
	uint64_t nb_point_location;
	uint64_t nb_points;
	std::string output_path;
};

#endif // _PLY_WRITER_H