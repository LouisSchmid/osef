#ifndef _POINT_CLOUD_PARSER_H
#define _POINT_CLOUD_PARSER_H

// Osef headers
#include "osefParser.h"

// Local headers
#include "writer/plyWriter.h"

// Class to read an osef stream and export it as a PointCloud (PLY format).
class PointCloudParser : public OsefParser {
    public:
	PointCloudParser(const std::string &inputPath, const std::string &outputPath, int maxScans,
			 bool autoReconnect = true);

	// Parse the input stream.
	// Overrided function.
	// returns:
	// 0 in case of success.
	// -1 in error.
	int parse() override;

    protected:
	// Parse the TLV frame and export data to the PLY.
	// Overrided function.
	// returns:
	// 0 in case of success.
	// -1 in error.
	int parseFrame(const Tlv::tlv_s *frame) override;

    private:
	int max_scans;
	int scan_counter;
	PlyWriter ply_writer;
};

#endif // _POINT_CLOUD_PARSER_H